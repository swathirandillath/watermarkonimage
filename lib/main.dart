import 'dart:async';
import 'package:camera/camera.dart';
import 'package:camerapermsn/watermark.dart';
import 'package:flutter/material.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final cameras = await availableCameras();

  final firstCamera = cameras.first;

  runApp(MaterialApp(
    title: 'image_watermark',
    theme: ThemeData(
      primarySwatch: Colors.blue,
    ),
    home: HomeScreen(firstCamera),
  ));
}
