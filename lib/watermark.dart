import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_watermark/image_watermark.dart';


class HomeScreen extends StatefulWidget {
  HomeScreen(this.firstCamera, {Key? key}) : super(key: key);
  CameraDescription firstCamera;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final ImagePicker _picker = ImagePicker();
  var imgBytes;
  var imgBytes2;
  var _image;
  var watermarkedImgBytes;
  bool isLoading = false;
  String watermarkText = "",
      imgname = "image not selected";
  List<bool> textOrImage = [true, false];


  pickImage2() async {
    XFile? image = await _picker.pickImage(
      source: ImageSource.gallery,
    );
    if (image != null) {
      _image = image;
      imgname = image.name;
      var t = await image.readAsBytes();
      imgBytes2 = Uint8List.fromList(t);
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('image_watermark'),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Center(
          child: Container(
            width: 600,
            child: Column(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          TakePictureScreen(
                            // Pass the appropriate camera to the TakePictureScreen widget.
                            camera: widget.firstCamera, onTap: (
                              fileName) async {
                            debugPrint("On Click : String: $fileName");
                            _image = fileName;
                            var t = await fileName.readAsBytes();
                            imgBytes = Uint8List.fromList(t);
                            setState(() {});
                          },
                          ),
                    ));
                  },
                  child: Container(
                      margin: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          border: Border.all(),
                          borderRadius: const BorderRadius.all(
                              Radius.circular(5))),
                      width: 600,
                      height: 250,
                      child: _image == null
                          ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Icon(Icons.add_a_photo),
                          SizedBox(
                            height: 10,
                          ),
                          Text('Click here to choose image')
                        ],
                      )
                          : Image.memory(imgBytes,
                          width: 600, height: 200, fit: BoxFit.fitHeight)),
                ),
                const SizedBox(
                  height: 10,
                ), Row(
                  children: [
                    ElevatedButton(
                        onPressed: pickImage2,
                        child: const Text('Select Watermark image')),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    setState(() {
                      isLoading = true;
                    });

                    watermarkedImgBytes =
                    await image_watermark.addImageWatermark(
                        imgBytes, //image bytes
                        imgBytes2,
                        imgHeight: 200,
                        imgWidth: 200,
                        dstY: 400,
                        dstX: 400);


                    setState(() {
                      isLoading = false;
                    });
                  },
                  child: const Text('Add Watermark'),
                ),
                const SizedBox(
                  height: 10,
                ),
                isLoading ? const CircularProgressIndicator() : Container(),
                watermarkedImgBytes == null
                    ? Container()
                    : Image.memory(watermarkedImgBytes),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
class TakePictureScreen extends StatefulWidget {
  TakePictureScreen({
    Key? key,
    required this.camera,
    required this.onTap,
  }) : super(key: key);

  final CameraDescription camera;
  Function(XFile) onTap;

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.medium,
    );

    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Take a picture')),

      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(_controller);
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {

          try {

            await _initializeControllerFuture;

            final image = await _controller.takePicture();


            widget.onTap(image);
            Navigator.pop(context);



          } catch (e) {
            print(e);
          }
        },
        child: const Icon(Icons.camera_alt),
      ),
    );
  }
}

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;

  const DisplayPictureScreen({Key? key, required this.imagePath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Display the Picture')),

      body: Image.file(File(imagePath)),
    );
  }
}

